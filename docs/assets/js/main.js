'use strict';

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

var Dropdown = /*#__PURE__*/function () {
  function Dropdown(element) {
    _classCallCheck(this, Dropdown);

    if (!element) return;
    this.$el = element;
    this.$head = this.$el.querySelector('[data-ddwn-head]');
    this.$body = this.$el.querySelector('[data-ddwn-body]');
    this.$items = this.$el.querySelectorAll('[data-ddwn-item]');
    this.current = this.$el.dataset['ddwn'] || this.$items[0].dataset['ddwn-item'] || 0;
    this.isShow = false;
    this.setup();
  }

  _createClass(Dropdown, [{
    key: "setup",
    value: function setup() {
      this.$el.addEventListener('mouseenter', this.handleHover.bind(this));
      this.$el.addEventListener('mouseleave', this.handleHover.bind(this));
    }
  }, {
    key: "updateShow",
    value: function updateShow() {
      if (this.isShow) {
        this.$el.classList.add('is-show');
      } else {
        this.$el.classList.remove('is-show');
      }
    }
  }, {
    key: "handleHover",
    value: function handleHover(e) {
      this.isShow = e.type === 'mouseenter' ? true : false;
      this.updateShow();
    }
  }]);

  return Dropdown;
}();
function installAllDdwn() {
  Array.prototype.forEach.call(document.querySelectorAll('[data-ddwn]'), function (item) {
    return new Dropdown(item);
  });
}

function expertSliderAll() {
  window.expertsSliders = [];
  Array.prototype.forEach.call(document.querySelectorAll('.js-slider-experts'), function (item) {
    var el = new Swiper(item.querySelector('.swiper-container'), {
      loop: false,
      spaceBetween: 16,
      slidesPerView: 4,
      navigation: {
        nextEl: item.querySelector('.slider-nav__item_next'),
        prevEl: item.querySelector('.slider-nav__item_prev')
      }
    });
    expertsSliders.push(el);
  });
}
function updateExpertSliderAll() {
  expertsSliders.forEach(function (item) {
    item.update();
  });
}

var Tabs = /*#__PURE__*/function () {
  function Tabs(element) {
    _classCallCheck(this, Tabs);

    if (!element) return;
    this.$el = element;
    this.$head = this.$el.querySelector('[data-tabs-head]');
    this.$body = this.$el.querySelector('[data-tabs-body]');
    this.$tabs = this.$el.querySelectorAll('[data-tabs-tab]');
    this.$panels = this.$el.querySelectorAll('[data-tabs-panel]');
    this.current = this.$el.dataset['tabs'] || this.$tabs[0].dataset['tab'] || 0;
    this.setup();
  }

  _createClass(Tabs, [{
    key: "setup",
    value: function setup() {
      var _this = this;

      this.$panels = Array.from(this.$panels).reduce(function (acc, el) {
        var id = el.dataset['tabsPanel'];
        acc[id] = el;
        return acc;
      }, {});
      this.$tabs = Array.from(this.$tabs).reduce(function (acc, el) {
        var id = el.dataset['tabsTab'];
        acc[id] = el;
        return acc;
      }, {});
      Object.values(this.$tabs).forEach(function (el) {
        el.addEventListener('click', _this.handleHover.bind(_this, el));
      });
      this.updateCurrent();
    }
  }, {
    key: "updateCurrent",
    value: function updateCurrent() {
      Object.values(this.$tabs).forEach(function (el) {
        return el.classList.remove('is-active');
      });
      Object.values(this.$panels).forEach(function (el) {
        return el.classList.remove('is-active');
      });
      if (this.$tabs[this.current]) this.$tabs[this.current].classList.add('is-active');
      if (this.$panels[this.current]) this.$panels[this.current].classList.add('is-active');
    }
  }, {
    key: "handleHover",
    value: function handleHover(el, e) {
      var id = el.dataset['tabsTab'];
      if (!id) return;
      this.current = id;
      this.updateCurrent();
    }
  }]);

  return Tabs;
}();
function installAllTabs() {
  Array.prototype.forEach.call(document.querySelectorAll('[data-tabs]'), function (item) {
    return new Tabs(item);
  });
}

function aboutSliderAll() {
  Array.prototype.forEach.call(document.querySelectorAll('.js-slider-about'), function (item) {
    new Swiper(item.querySelector('.swiper-container'), {
      loop: false,
      spaceBetween: 0,
      slidesPerView: 1,
      effect: 'fade',
      navigation: {
        nextEl: item.querySelector('.slider-nav__item_next'),
        prevEl: item.querySelector('.slider-nav__item_prev')
      }
    });
  });
}

var Faq = /*#__PURE__*/function () {
  function Faq(element) {
    _classCallCheck(this, Faq);

    if (!element) return;
    this.$el = element;
    this.$head = this.$el.querySelector('[data-faq-head]');
    this.$body = this.$el.querySelector('[data-faq-body]');
    this.isShow = this.$el.dataset['faq'] == 'true' ? true : false;
    this.setup();
  }

  _createClass(Faq, [{
    key: "setup",
    value: function setup() {
      this.$el.addEventListener('click', this.handleClick.bind(this));
      this.updateShow();
    }
  }, {
    key: "updateShow",
    value: function updateShow() {
      if (this.isShow) {
        this.$el.classList.add('is-active');
      } else {
        this.$el.classList.remove('is-active');
      }
    }
  }, {
    key: "handleClick",
    value: function handleClick(e) {
      this.isShow = this.isShow ? false : true;
      this.updateShow();
    }
  }]);

  return Faq;
}();
function installAllFaq() {
  Array.prototype.forEach.call(document.querySelectorAll('[data-faq]'), function (item) {
    return new Faq(item);
  });
}

function articleSliderAll() {
  Array.prototype.forEach.call(document.querySelectorAll('.js-slider-article'), function (item) {
    new Swiper(item, {
      loop: false,
      spaceBetween: 0,
      slidesPerView: 1,
      effect: 'fade',
      navigation: {
        nextEl: item.querySelector('.slider-nav__item_next'),
        prevEl: item.querySelector('.slider-nav__item_prev')
      }
    });
  });
}

function installAllGallery() {
  Array.prototype.forEach.call(document.querySelectorAll('.js-lightgallery'), function (item) {
    lightGallery(item);
  });
}

var ModalBox = /*#__PURE__*/function () {
  function ModalBox(btnsTrigger) {
    _classCallCheck(this, ModalBox);

    this.opts = {
      classActive: "show",
      extraClassHide: "hide",
      timeoutClose: 200,
      widthScrollbar: 0
    };
    this.btns = document.querySelectorAll(btnsTrigger);
    this.modals = document.querySelectorAll("[data-modal]");
    this.btnsClose = document.querySelectorAll("[data-modal-close]");
    this.lastElementBeforeModal = null;
    this.init();
  }

  _createClass(ModalBox, [{
    key: "init",
    value: function init() {
      // console.log(this);
      this.events();
      this.opts.widthScrollbar = this.widthScrollbar();
    }
  }, {
    key: "events",
    value: function events() {
      var _this = this;

      [].forEach.call(this.btns, function (btn) {
        btn.addEventListener("click", function (e) {
          return _this.openModal(btn.dataset.modalTarget, e);
        });
      });
      [].forEach.call(this.btnsClose, function (btn) {
        btn.addEventListener("click", function (e) {
          return _this.closeModal(btn.closest("[data-modal]"), e);
        });
      });
      [].forEach.call(this.modals, function (modal) {
        modal.addEventListener("click", function (e) {
          return _this.backDrop(modal, e);
        });
      });
      document.addEventListener("keydown", this.handlerKeyDownModal);
    }
  }, {
    key: "openModal",
    value: function openModal(modalTarget, event) {
      var modal = document.querySelector("[data-modal='".concat(modalTarget, "']"));
      if (!modal) return;
      this.lastElementBeforeModal = document.activeElement;
      modal.classList.add(this.opts.classActive);
      modal.setAttribute("tabindex", 0);
      modal.focus();
      document.body.style.overflow = "hidden";
      document.body.style.marginRight = "".concat(this.opts.widthScrollbar, "px");
    }
  }, {
    key: "closeModal",
    value: function closeModal(modal, event) {
      var _this2 = this;

      modal.classList.add(this.opts.extraClassHide);
      this.lastElementBeforeModal.focus();
      setTimeout(function () {
        modal.classList.remove(_this2.opts.classActive, _this2.opts.extraClassHide);
        document.body.style.overflow = "initial";
        document.body.style.marginRight = "0";
        modal.removeAttribute("tabindex");
      }, this.opts.timeoutClose);
    }
  }, {
    key: "closeModalAll",
    value: function closeModalAll() {
      var _this3 = this;

      [].forEach.call(this.modals, function (modal) {
        _this3.closeModal(modal);
      });
    }
  }, {
    key: "backDrop",
    value: function backDrop(modal, event) {
      if (event.target === event.currentTarget) {
        this.closeModal(modal);
      }
    }
  }, {
    key: "handlerKeyDownModal",
    value: function handlerKeyDownModal(event) {
      if (event.keyCode === 27) {
        this.closeModalAll();
      }
    }
  }, {
    key: "widthScrollbar",
    value: function widthScrollbar() {
      var div = document.createElement("div");
      div.style.overflowY = "scroll";
      div.style.width = "50px";
      div.style.height = "50px"; // мы должны вставить элемент в документ, иначе размеры будут равны 0

      document.body.append(div);
      var scrollWidth = div.offsetWidth - div.clientWidth;
      div.remove();
      return scrollWidth;
    }
  }]);

  return ModalBox;
}();
function installModal() {
  window.modalBox = new ModalBox("[data-modal-target]");
}

function headerHumburger() {
  var headerMenu = document.querySelector('.header__menu');
  var headerHumburger = document.querySelector('.header__humburger');
  headerHumburger.addEventListener('click', function () {
    if (headerMenu.classList.contains('is-active')) {
      document.body.style.overflow = '';
    } else {
      document.body.style.overflow = 'hidden';
    }

    headerHumburger.classList.toggle('is-active');
    headerMenu.classList.toggle('is-active');
  });
}

function countDownTimer() {
  var countDown = new Date(2020, 8, 20, 12, 0, 0).getTime();
  var second = 1000,
      minute = second * 60,
      hour = minute * 60,
      day = hour * 24;

  var isZero = function isZero(val) {
    return val < 10 ? '0' + val : val;
  };

  var timerDays = document.getElementById('timerDays');
  var timerHours = document.getElementById('timerHours');
  var timerMinutes = document.getElementById('timerMinutes');
  var timerSeconds = document.getElementById('timerSeconds');
  if (!timerDays && !timerHours && !timerMinutes && !timerSeconds) return;
  var x = setInterval(function () {
    var now = new Date().getTime();
    var distance = countDown - now;
    timerDays.innerText = isZero(Math.floor(distance / day));
    timerHours.innerText = isZero(Math.floor(distance % day / hour));
    timerMinutes.innerText = isZero(Math.floor(distance % hour / minute));
    timerSeconds.innerText = isZero(Math.floor(distance % minute / second));

    if (distance < 0) {
      clearInterval(x);
    }
  }, second);
}

window.addEventListener('DOMContentLoaded', function (event) {
  installAllDdwn();
  installAllTabs();
  aboutSliderAll();
  installAllFaq();
  articleSliderAll();
  installAllGallery();
  installModal();
  headerHumburger();
  countDownTimer();
  setTimeout(function () {
    expertSliderAll();
  }, 0);
  Array.prototype.forEach.call(document.querySelectorAll('[data-modal-target="live"]'), function (item) {
    item.addEventListener('click', function () {
      updateExpertSliderAll();
    });
  });
  Array.prototype.forEach.call(document.querySelectorAll('[data-tabs-tab]'), function (item) {
    item.addEventListener('click', function () {
      updateExpertSliderAll();
    });
  });
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXMiOlsiLi4vY29tcG9uZW50cy9kcm9wZG93bi9pbmRleC5qcyIsIi4uL2NvbXBvbmVudHMvZXhwZXJ0cy1zbGlkZXIvaW5kZXguanMiLCIuLi9jb21wb25lbnRzL3RhYnMvaW5kZXguanMiLCIuLi9jb21wb25lbnRzL2Fib3V0LWJsb2NrL2luZGV4LmpzIiwiLi4vY29tcG9uZW50cy9mYXEvaW5kZXguanMiLCIuLi9jb21wb25lbnRzL2FydGljbGUvaW5kZXguanMiLCIuLi9jb21wb25lbnRzL2dhbGxlcnkvaW5kZXguanMiLCIuLi9jb21wb25lbnRzL21vZGFsL2luZGV4LmpzIiwiLi4vY29tcG9uZW50cy9oZWFkZXIvaW5kZXguanMiLCIuLi9jb21wb25lbnRzL3RpbWVyL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjbGFzcyBEcm9wZG93biB7XG4gIGNvbnN0cnVjdG9yKGVsZW1lbnQpIHtcbiAgICBpZiAoIWVsZW1lbnQpIHJldHVyblxuICAgIHRoaXMuJGVsID0gZWxlbWVudFxuICAgIHRoaXMuJGhlYWQgPSB0aGlzLiRlbC5xdWVyeVNlbGVjdG9yKCdbZGF0YS1kZHduLWhlYWRdJylcbiAgICB0aGlzLiRib2R5ID0gdGhpcy4kZWwucXVlcnlTZWxlY3RvcignW2RhdGEtZGR3bi1ib2R5XScpXG4gICAgdGhpcy4kaXRlbXMgPSB0aGlzLiRlbC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1kZHduLWl0ZW1dJylcblxuICAgIHRoaXMuY3VycmVudCA9IHRoaXMuJGVsLmRhdGFzZXRbJ2Rkd24nXSB8fCB0aGlzLiRpdGVtc1swXS5kYXRhc2V0WydkZHduLWl0ZW0nXSB8fCAwXG4gICAgdGhpcy5pc1Nob3cgPSBmYWxzZVxuXG4gICAgdGhpcy5zZXR1cCgpXG4gIH1cblxuICBzZXR1cCgpIHtcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWVudGVyJywgdGhpcy5oYW5kbGVIb3Zlci5iaW5kKHRoaXMpKVxuICAgIHRoaXMuJGVsLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLCB0aGlzLmhhbmRsZUhvdmVyLmJpbmQodGhpcykpXG4gIH1cblxuICB1cGRhdGVTaG93KCkge1xuICAgIGlmICh0aGlzLmlzU2hvdykge1xuICAgICAgdGhpcy4kZWwuY2xhc3NMaXN0LmFkZCgnaXMtc2hvdycpXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuJGVsLmNsYXNzTGlzdC5yZW1vdmUoJ2lzLXNob3cnKVxuICAgIH1cbiAgfVxuXG4gIGhhbmRsZUhvdmVyKGUpIHtcbiAgICB0aGlzLmlzU2hvdyA9IGUudHlwZSA9PT0gJ21vdXNlZW50ZXInID8gdHJ1ZSA6IGZhbHNlXG4gICAgdGhpcy51cGRhdGVTaG93KClcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5zdGFsbEFsbERkd24oKSB7XG4gIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtZGR3bl0nKSxcbiAgICBpdGVtID0+IG5ldyBEcm9wZG93bihpdGVtKVxuICApXG59XG4iLCJleHBvcnQgZnVuY3Rpb24gZXhwZXJ0U2xpZGVyQWxsKCkge1xuICB3aW5kb3cuZXhwZXJ0c1NsaWRlcnMgPSBbXVxuICBBcnJheS5wcm90b3R5cGUuZm9yRWFjaC5jYWxsKFxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qcy1zbGlkZXItZXhwZXJ0cycpLFxuICAgIGl0ZW0gPT4ge1xuICAgICAgY29uc3QgZWwgPSBuZXcgU3dpcGVyKGl0ZW0ucXVlcnlTZWxlY3RvcignLnN3aXBlci1jb250YWluZXInKSwge1xuICAgICAgICBsb29wOiBmYWxzZSxcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAxNixcbiAgICAgICAgc2xpZGVzUGVyVmlldzogNCxcbiAgICAgICAgbmF2aWdhdGlvbjoge1xuICAgICAgICAgIG5leHRFbDogaXRlbS5xdWVyeVNlbGVjdG9yKCcuc2xpZGVyLW5hdl9faXRlbV9uZXh0JyksXG4gICAgICAgICAgcHJldkVsOiBpdGVtLnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZXItbmF2X19pdGVtX3ByZXYnKSxcbiAgICAgICAgfSxcbiAgICAgIH0pXG4gICAgICBleHBlcnRzU2xpZGVycy5wdXNoKGVsKVxuICAgIH1cbiAgKVxufVxuXG5leHBvcnQgZnVuY3Rpb24gdXBkYXRlRXhwZXJ0U2xpZGVyQWxsKCkge1xuICBleHBlcnRzU2xpZGVycy5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgIGl0ZW0udXBkYXRlKClcbiAgfSlcbn1cblxuIiwiZXhwb3J0IGNsYXNzIFRhYnMge1xuICBjb25zdHJ1Y3RvcihlbGVtZW50KSB7XG4gICAgaWYgKCFlbGVtZW50KSByZXR1cm5cbiAgICB0aGlzLiRlbCA9IGVsZW1lbnRcbiAgICB0aGlzLiRoZWFkID0gdGhpcy4kZWwucXVlcnlTZWxlY3RvcignW2RhdGEtdGFicy1oZWFkXScpXG4gICAgdGhpcy4kYm9keSA9IHRoaXMuJGVsLnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLXRhYnMtYm9keV0nKVxuICAgIHRoaXMuJHRhYnMgPSB0aGlzLiRlbC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS10YWJzLXRhYl0nKVxuICAgIHRoaXMuJHBhbmVscyA9IHRoaXMuJGVsLnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLXRhYnMtcGFuZWxdJylcblxuICAgIHRoaXMuY3VycmVudCA9IHRoaXMuJGVsLmRhdGFzZXRbJ3RhYnMnXSB8fCB0aGlzLiR0YWJzWzBdLmRhdGFzZXRbJ3RhYiddIHx8IDBcblxuICAgIHRoaXMuc2V0dXAoKVxuICB9XG5cbiAgc2V0dXAoKSB7XG4gICAgdGhpcy4kcGFuZWxzID0gQXJyYXkuZnJvbSh0aGlzLiRwYW5lbHMpLnJlZHVjZSgoYWNjLCBlbCkgPT4ge1xuICAgICAgY29uc3QgaWQgPSBlbC5kYXRhc2V0Wyd0YWJzUGFuZWwnXVxuICAgICAgYWNjW2lkXSA9IGVsXG4gICAgICByZXR1cm4gYWNjXG4gICAgfSwge30pXG4gICAgdGhpcy4kdGFicyA9IEFycmF5LmZyb20odGhpcy4kdGFicykucmVkdWNlKChhY2MsIGVsKSA9PiB7XG4gICAgICBjb25zdCBpZCA9IGVsLmRhdGFzZXRbJ3RhYnNUYWInXVxuICAgICAgYWNjW2lkXSA9IGVsXG4gICAgICByZXR1cm4gYWNjXG4gICAgfSwge30pXG4gICAgT2JqZWN0LnZhbHVlcyh0aGlzLiR0YWJzKS5mb3JFYWNoKGVsID0+IHtcbiAgICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgdGhpcy5oYW5kbGVIb3Zlci5iaW5kKHRoaXMsIGVsKSlcbiAgICB9KVxuICAgIHRoaXMudXBkYXRlQ3VycmVudCgpXG4gIH1cblxuICB1cGRhdGVDdXJyZW50KCkge1xuICAgIE9iamVjdC52YWx1ZXModGhpcy4kdGFicykuZm9yRWFjaChlbCA9PiBlbC5jbGFzc0xpc3QucmVtb3ZlKCdpcy1hY3RpdmUnKSlcbiAgICBPYmplY3QudmFsdWVzKHRoaXMuJHBhbmVscykuZm9yRWFjaChlbCA9PiBlbC5jbGFzc0xpc3QucmVtb3ZlKCdpcy1hY3RpdmUnKSlcbiAgICBpZiAodGhpcy4kdGFic1t0aGlzLmN1cnJlbnRdKSB0aGlzLiR0YWJzW3RoaXMuY3VycmVudF0uY2xhc3NMaXN0LmFkZCgnaXMtYWN0aXZlJylcbiAgICBpZiAodGhpcy4kcGFuZWxzW3RoaXMuY3VycmVudF0pIHRoaXMuJHBhbmVsc1t0aGlzLmN1cnJlbnRdLmNsYXNzTGlzdC5hZGQoJ2lzLWFjdGl2ZScpXG4gIH1cblxuICBoYW5kbGVIb3ZlcihlbCwgZSkge1xuICAgIGNvbnN0IGlkID0gZWwuZGF0YXNldFsndGFic1RhYiddXG4gICAgaWYgKCFpZCkgcmV0dXJuO1xuICAgIHRoaXMuY3VycmVudCA9IGlkXG4gICAgdGhpcy51cGRhdGVDdXJyZW50KClcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaW5zdGFsbEFsbFRhYnMoKSB7XG4gIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtdGFic10nKSxcbiAgICBpdGVtID0+IG5ldyBUYWJzKGl0ZW0pXG4gIClcbn1cbiIsImV4cG9ydCBmdW5jdGlvbiBhYm91dFNsaWRlckFsbCgpIHtcbiAgQXJyYXkucHJvdG90eXBlLmZvckVhY2guY2FsbChcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanMtc2xpZGVyLWFib3V0JyksXG4gICAgaXRlbSA9PiB7XG4gICAgICBuZXcgU3dpcGVyKGl0ZW0ucXVlcnlTZWxlY3RvcignLnN3aXBlci1jb250YWluZXInKSwge1xuICAgICAgICBsb29wOiBmYWxzZSxcbiAgICAgICAgc3BhY2VCZXR3ZWVuOiAwLFxuICAgICAgICBzbGlkZXNQZXJWaWV3OiAxLFxuICAgICAgICBlZmZlY3Q6ICdmYWRlJyxcbiAgICAgICAgbmF2aWdhdGlvbjoge1xuICAgICAgICAgIG5leHRFbDogaXRlbS5xdWVyeVNlbGVjdG9yKCcuc2xpZGVyLW5hdl9faXRlbV9uZXh0JyksXG4gICAgICAgICAgcHJldkVsOiBpdGVtLnF1ZXJ5U2VsZWN0b3IoJy5zbGlkZXItbmF2X19pdGVtX3ByZXYnKSxcbiAgICAgICAgfSxcbiAgICAgIH0pXG4gICAgfVxuICApXG59XG4iLCJleHBvcnQgY2xhc3MgRmFxIHtcbiAgY29uc3RydWN0b3IoZWxlbWVudCkge1xuICAgIGlmICghZWxlbWVudCkgcmV0dXJuXG4gICAgdGhpcy4kZWwgPSBlbGVtZW50XG4gICAgdGhpcy4kaGVhZCA9IHRoaXMuJGVsLnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWZhcS1oZWFkXScpXG4gICAgdGhpcy4kYm9keSA9IHRoaXMuJGVsLnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWZhcS1ib2R5XScpXG5cbiAgICB0aGlzLmlzU2hvdyA9IHRoaXMuJGVsLmRhdGFzZXRbJ2ZhcSddID09ICd0cnVlJyA/IHRydWUgOiBmYWxzZVxuXG4gICAgdGhpcy5zZXR1cCgpXG4gIH1cblxuICBzZXR1cCgpIHtcbiAgICB0aGlzLiRlbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuaGFuZGxlQ2xpY2suYmluZCh0aGlzKSlcbiAgICB0aGlzLnVwZGF0ZVNob3coKVxuICB9XG5cbiAgdXBkYXRlU2hvdygpIHtcbiAgICBpZiAodGhpcy5pc1Nob3cpIHtcbiAgICAgIHRoaXMuJGVsLmNsYXNzTGlzdC5hZGQoJ2lzLWFjdGl2ZScpXG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuJGVsLmNsYXNzTGlzdC5yZW1vdmUoJ2lzLWFjdGl2ZScpXG4gICAgfVxuICB9XG5cbiAgaGFuZGxlQ2xpY2soZSkge1xuICAgIHRoaXMuaXNTaG93ID0gdGhpcy5pc1Nob3cgPyBmYWxzZSA6IHRydWVcbiAgICB0aGlzLnVwZGF0ZVNob3coKVxuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBpbnN0YWxsQWxsRmFxKCkge1xuICBBcnJheS5wcm90b3R5cGUuZm9yRWFjaC5jYWxsKFxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ1tkYXRhLWZhcV0nKSxcbiAgICBpdGVtID0+IG5ldyBGYXEoaXRlbSlcbiAgKVxufVxuIiwiZXhwb3J0IGZ1bmN0aW9uIGFydGljbGVTbGlkZXJBbGwoKSB7XG4gIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmpzLXNsaWRlci1hcnRpY2xlJyksXG4gICAgaXRlbSA9PiB7XG4gICAgICBuZXcgU3dpcGVyKGl0ZW0sIHtcbiAgICAgICAgbG9vcDogZmFsc2UsXG4gICAgICAgIHNwYWNlQmV0d2VlbjogMCxcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMSxcbiAgICAgICAgZWZmZWN0OiAnZmFkZScsXG4gICAgICAgIG5hdmlnYXRpb246IHtcbiAgICAgICAgICBuZXh0RWw6IGl0ZW0ucXVlcnlTZWxlY3RvcignLnNsaWRlci1uYXZfX2l0ZW1fbmV4dCcpLFxuICAgICAgICAgIHByZXZFbDogaXRlbS5xdWVyeVNlbGVjdG9yKCcuc2xpZGVyLW5hdl9faXRlbV9wcmV2JyksXG4gICAgICAgIH0sXG4gICAgICB9KVxuICAgIH1cbiAgKVxufVxuIiwiZXhwb3J0IGZ1bmN0aW9uIGluc3RhbGxBbGxHYWxsZXJ5KCkge1xuICBBcnJheS5wcm90b3R5cGUuZm9yRWFjaC5jYWxsKFxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5qcy1saWdodGdhbGxlcnknKSxcbiAgICBpdGVtID0+IHtcbiAgICAgIGxpZ2h0R2FsbGVyeShpdGVtKVxuICAgIH1cbiAgKVxufVxuIiwiZXhwb3J0IGNsYXNzIE1vZGFsQm94IHtcbiAgY29uc3RydWN0b3IoYnRuc1RyaWdnZXIpIHtcbiAgICB0aGlzLm9wdHMgPSB7XG4gICAgICBjbGFzc0FjdGl2ZTogXCJzaG93XCIsXG4gICAgICBleHRyYUNsYXNzSGlkZTogXCJoaWRlXCIsXG4gICAgICB0aW1lb3V0Q2xvc2U6IDIwMCxcbiAgICAgIHdpZHRoU2Nyb2xsYmFyOiAwLFxuICAgIH07XG4gICAgdGhpcy5idG5zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChidG5zVHJpZ2dlcik7XG4gICAgdGhpcy5tb2RhbHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtbW9kYWxdXCIpO1xuICAgIHRoaXMuYnRuc0Nsb3NlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltkYXRhLW1vZGFsLWNsb3NlXVwiKTtcbiAgICB0aGlzLmxhc3RFbGVtZW50QmVmb3JlTW9kYWwgPSBudWxsO1xuXG4gICAgdGhpcy5pbml0KCk7XG4gIH1cblxuICBpbml0KCkge1xuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMpO1xuICAgIHRoaXMuZXZlbnRzKCk7XG4gICAgdGhpcy5vcHRzLndpZHRoU2Nyb2xsYmFyID0gdGhpcy53aWR0aFNjcm9sbGJhcigpO1xuICB9O1xuXG4gIGV2ZW50cygpIHtcbiAgICBbXS5mb3JFYWNoLmNhbGwodGhpcy5idG5zLCAoYnRuKSA9PiB7XG4gICAgICBidG4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIChlKSA9PlxuICAgICAgICB0aGlzLm9wZW5Nb2RhbChidG4uZGF0YXNldC5tb2RhbFRhcmdldCwgZSlcbiAgICAgICk7XG4gICAgfSk7XG5cbiAgICBbXS5mb3JFYWNoLmNhbGwodGhpcy5idG5zQ2xvc2UsIChidG4pID0+IHtcbiAgICAgIGJ0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKGUpID0+XG4gICAgICAgIHRoaXMuY2xvc2VNb2RhbChidG4uY2xvc2VzdChcIltkYXRhLW1vZGFsXVwiKSwgZSlcbiAgICAgICk7XG4gICAgfSk7XG5cbiAgICBbXS5mb3JFYWNoLmNhbGwodGhpcy5tb2RhbHMsIChtb2RhbCkgPT4ge1xuICAgICAgbW9kYWwuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIChlKSA9PiB0aGlzLmJhY2tEcm9wKG1vZGFsLCBlKSk7XG4gICAgfSk7XG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcImtleWRvd25cIiwgdGhpcy5oYW5kbGVyS2V5RG93bk1vZGFsKTtcbiAgfTtcblxuICBvcGVuTW9kYWwobW9kYWxUYXJnZXQsIGV2ZW50KSB7XG4gICAgY29uc3QgbW9kYWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGBbZGF0YS1tb2RhbD0nJHttb2RhbFRhcmdldH0nXWApO1xuICAgIGlmICghbW9kYWwpIHJldHVybjtcbiAgICB0aGlzLmxhc3RFbGVtZW50QmVmb3JlTW9kYWwgPSBkb2N1bWVudC5hY3RpdmVFbGVtZW50O1xuICAgIG1vZGFsLmNsYXNzTGlzdC5hZGQodGhpcy5vcHRzLmNsYXNzQWN0aXZlKTtcbiAgICBtb2RhbC5zZXRBdHRyaWJ1dGUoXCJ0YWJpbmRleFwiLCAwKTtcbiAgICBtb2RhbC5mb2N1cygpO1xuICAgIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSBcImhpZGRlblwiO1xuICAgIGRvY3VtZW50LmJvZHkuc3R5bGUubWFyZ2luUmlnaHQgPSBgJHt0aGlzLm9wdHMud2lkdGhTY3JvbGxiYXJ9cHhgO1xuICB9O1xuXG4gIGNsb3NlTW9kYWwobW9kYWwsIGV2ZW50KSB7XG4gICAgbW9kYWwuY2xhc3NMaXN0LmFkZCh0aGlzLm9wdHMuZXh0cmFDbGFzc0hpZGUpO1xuICAgIHRoaXMubGFzdEVsZW1lbnRCZWZvcmVNb2RhbC5mb2N1cygpO1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgbW9kYWwuY2xhc3NMaXN0LnJlbW92ZSh0aGlzLm9wdHMuY2xhc3NBY3RpdmUsIHRoaXMub3B0cy5leHRyYUNsYXNzSGlkZSk7XG4gICAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93ID0gXCJpbml0aWFsXCI7XG4gICAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm1hcmdpblJpZ2h0ID0gXCIwXCI7XG4gICAgICBtb2RhbC5yZW1vdmVBdHRyaWJ1dGUoXCJ0YWJpbmRleFwiKTtcbiAgICB9LCB0aGlzLm9wdHMudGltZW91dENsb3NlKTtcbiAgfTtcblxuICBjbG9zZU1vZGFsQWxsKCkge1xuICAgIFtdLmZvckVhY2guY2FsbCh0aGlzLm1vZGFscywgKG1vZGFsKSA9PiB7XG4gICAgICB0aGlzLmNsb3NlTW9kYWwobW9kYWwpO1xuICAgIH0pO1xuICB9O1xuXG4gIGJhY2tEcm9wKG1vZGFsLCBldmVudCkge1xuICAgIGlmIChldmVudC50YXJnZXQgPT09IGV2ZW50LmN1cnJlbnRUYXJnZXQpIHtcbiAgICAgIHRoaXMuY2xvc2VNb2RhbChtb2RhbCk7XG4gICAgfVxuICB9O1xuXG4gIGhhbmRsZXJLZXlEb3duTW9kYWwoZXZlbnQpIHtcbiAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMjcpIHtcbiAgICAgIHRoaXMuY2xvc2VNb2RhbEFsbCgpO1xuICAgIH1cbiAgfTtcblxuICB3aWR0aFNjcm9sbGJhcigpIHtcbiAgICBsZXQgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcblxuICAgIGRpdi5zdHlsZS5vdmVyZmxvd1kgPSBcInNjcm9sbFwiO1xuICAgIGRpdi5zdHlsZS53aWR0aCA9IFwiNTBweFwiO1xuICAgIGRpdi5zdHlsZS5oZWlnaHQgPSBcIjUwcHhcIjtcblxuICAgIC8vINC80Ysg0LTQvtC70LbQvdGLINCy0YHRgtCw0LLQuNGC0Ywg0Y3Qu9C10LzQtdC90YIg0LIg0LTQvtC60YPQvNC10L3Rgiwg0LjQvdCw0YfQtSDRgNCw0LfQvNC10YDRiyDQsdGD0LTRg9GCINGA0LDQstC90YsgMFxuICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kKGRpdik7XG4gICAgbGV0IHNjcm9sbFdpZHRoID0gZGl2Lm9mZnNldFdpZHRoIC0gZGl2LmNsaWVudFdpZHRoO1xuXG4gICAgZGl2LnJlbW92ZSgpO1xuXG4gICAgcmV0dXJuIHNjcm9sbFdpZHRoO1xuICB9O1xufVxuXG5cbmV4cG9ydCBmdW5jdGlvbiBpbnN0YWxsTW9kYWwoKSB7XG4gIHdpbmRvdy5tb2RhbEJveCA9IG5ldyBNb2RhbEJveChcIltkYXRhLW1vZGFsLXRhcmdldF1cIik7XG59XG4iLCJleHBvcnQgZnVuY3Rpb24gaGVhZGVySHVtYnVyZ2VyKCkge1xuICBjb25zdCBoZWFkZXJNZW51ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmhlYWRlcl9fbWVudScpO1xuICBjb25zdCBoZWFkZXJIdW1idXJnZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuaGVhZGVyX19odW1idXJnZXInKTtcblxuICBoZWFkZXJIdW1idXJnZXIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XG4gICAgaWYgKGhlYWRlck1lbnUuY2xhc3NMaXN0LmNvbnRhaW5zKCdpcy1hY3RpdmUnKSkge1xuICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS5vdmVyZmxvdyA9ICcnXG4gICAgfSBlbHNlIHtcbiAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJ1xuICAgIH1cbiAgICBoZWFkZXJIdW1idXJnZXIuY2xhc3NMaXN0LnRvZ2dsZSgnaXMtYWN0aXZlJylcbiAgICBoZWFkZXJNZW51LmNsYXNzTGlzdC50b2dnbGUoJ2lzLWFjdGl2ZScpXG4gIH0pXG59XG4iLCJleHBvcnQgZnVuY3Rpb24gY291bnREb3duVGltZXIoKSB7XG4gIGNvbnN0IGNvdW50RG93biA9IG5ldyBEYXRlKDIwMjAsIDgsIDIwLCAxMiwgMCwgMCkuZ2V0VGltZSgpXG4gIGNvbnN0IHNlY29uZCA9IDEwMDAsXG4gICAgICAgIG1pbnV0ZSA9IHNlY29uZCAqIDYwLFxuICAgICAgICBob3VyID0gbWludXRlICogNjAsXG4gICAgICAgIGRheSA9IGhvdXIgKiAyNDtcblxuICBjb25zdCBpc1plcm8gPSAodmFsKSA9PiB2YWwgPCAxMCA/ICcwJyArIHZhbCA6IHZhbFxuXG4gIGNvbnN0IHRpbWVyRGF5cyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0aW1lckRheXMnKVxuICBjb25zdCB0aW1lckhvdXJzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3RpbWVySG91cnMnKVxuICBjb25zdCB0aW1lck1pbnV0ZXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgndGltZXJNaW51dGVzJylcbiAgY29uc3QgdGltZXJTZWNvbmRzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3RpbWVyU2Vjb25kcycpXG5cbiAgaWYgKCF0aW1lckRheXMgJiYgIXRpbWVySG91cnMgJiYgIXRpbWVyTWludXRlcyAmJiAhdGltZXJTZWNvbmRzKSByZXR1cm47XG5cbiAgbGV0IHggPSBzZXRJbnRlcnZhbChmdW5jdGlvbigpIHtcblxuICBsZXQgbm93ID0gbmV3IERhdGUoKS5nZXRUaW1lKClcbiAgbGV0IGRpc3RhbmNlID0gY291bnREb3duIC0gbm93O1xuXG4gIHRpbWVyRGF5cy5pbm5lclRleHQgPSBpc1plcm8oTWF0aC5mbG9vcihkaXN0YW5jZSAvIChkYXkpKSlcbiAgdGltZXJIb3Vycy5pbm5lclRleHQgPSBpc1plcm8oTWF0aC5mbG9vcigoZGlzdGFuY2UgJSAoZGF5KSkgLyAoaG91cikpKVxuICB0aW1lck1pbnV0ZXMuaW5uZXJUZXh0ID0gaXNaZXJvKE1hdGguZmxvb3IoKGRpc3RhbmNlICUgKGhvdXIpKSAvIChtaW51dGUpKSlcbiAgdGltZXJTZWNvbmRzLmlubmVyVGV4dCA9IGlzWmVybyhNYXRoLmZsb29yKChkaXN0YW5jZSAlIChtaW51dGUpKSAvIHNlY29uZCkpXG5cbiAgaWYgKGRpc3RhbmNlIDwgMCkge1xuICAgIGNsZWFySW50ZXJ2YWwoeCk7XG4gIH1cblxuICB9LCBzZWNvbmQpXG59XG4iXSwibmFtZXMiOlsiRHJvcGRvd24iLCJlbGVtZW50IiwiJGVsIiwiJGhlYWQiLCJxdWVyeVNlbGVjdG9yIiwiJGJvZHkiLCIkaXRlbXMiLCJxdWVyeVNlbGVjdG9yQWxsIiwiY3VycmVudCIsImRhdGFzZXQiLCJpc1Nob3ciLCJzZXR1cCIsImFkZEV2ZW50TGlzdGVuZXIiLCJoYW5kbGVIb3ZlciIsImJpbmQiLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmUiLCJlIiwidHlwZSIsInVwZGF0ZVNob3ciLCJpbnN0YWxsQWxsRGR3biIsIkFycmF5IiwicHJvdG90eXBlIiwiZm9yRWFjaCIsImNhbGwiLCJkb2N1bWVudCIsIml0ZW0iLCJleHBlcnRTbGlkZXJBbGwiLCJ3aW5kb3ciLCJleHBlcnRzU2xpZGVycyIsImVsIiwiU3dpcGVyIiwibG9vcCIsInNwYWNlQmV0d2VlbiIsInNsaWRlc1BlclZpZXciLCJuYXZpZ2F0aW9uIiwibmV4dEVsIiwicHJldkVsIiwicHVzaCIsInVwZGF0ZUV4cGVydFNsaWRlckFsbCIsInVwZGF0ZSIsIlRhYnMiLCIkdGFicyIsIiRwYW5lbHMiLCJmcm9tIiwicmVkdWNlIiwiYWNjIiwiaWQiLCJPYmplY3QiLCJ2YWx1ZXMiLCJ1cGRhdGVDdXJyZW50IiwiaW5zdGFsbEFsbFRhYnMiLCJhYm91dFNsaWRlckFsbCIsImVmZmVjdCIsIkZhcSIsImhhbmRsZUNsaWNrIiwiaW5zdGFsbEFsbEZhcSIsImFydGljbGVTbGlkZXJBbGwiLCJpbnN0YWxsQWxsR2FsbGVyeSIsImxpZ2h0R2FsbGVyeSIsIk1vZGFsQm94IiwiYnRuc1RyaWdnZXIiLCJvcHRzIiwiY2xhc3NBY3RpdmUiLCJleHRyYUNsYXNzSGlkZSIsInRpbWVvdXRDbG9zZSIsIndpZHRoU2Nyb2xsYmFyIiwiYnRucyIsIm1vZGFscyIsImJ0bnNDbG9zZSIsImxhc3RFbGVtZW50QmVmb3JlTW9kYWwiLCJpbml0IiwiZXZlbnRzIiwiYnRuIiwib3Blbk1vZGFsIiwibW9kYWxUYXJnZXQiLCJjbG9zZU1vZGFsIiwiY2xvc2VzdCIsIm1vZGFsIiwiYmFja0Ryb3AiLCJoYW5kbGVyS2V5RG93bk1vZGFsIiwiZXZlbnQiLCJhY3RpdmVFbGVtZW50Iiwic2V0QXR0cmlidXRlIiwiZm9jdXMiLCJib2R5Iiwic3R5bGUiLCJvdmVyZmxvdyIsIm1hcmdpblJpZ2h0Iiwic2V0VGltZW91dCIsInJlbW92ZUF0dHJpYnV0ZSIsInRhcmdldCIsImN1cnJlbnRUYXJnZXQiLCJrZXlDb2RlIiwiY2xvc2VNb2RhbEFsbCIsImRpdiIsImNyZWF0ZUVsZW1lbnQiLCJvdmVyZmxvd1kiLCJ3aWR0aCIsImhlaWdodCIsImFwcGVuZCIsInNjcm9sbFdpZHRoIiwib2Zmc2V0V2lkdGgiLCJjbGllbnRXaWR0aCIsImluc3RhbGxNb2RhbCIsIm1vZGFsQm94IiwiaGVhZGVySHVtYnVyZ2VyIiwiaGVhZGVyTWVudSIsImNvbnRhaW5zIiwidG9nZ2xlIiwiY291bnREb3duVGltZXIiLCJjb3VudERvd24iLCJEYXRlIiwiZ2V0VGltZSIsInNlY29uZCIsIm1pbnV0ZSIsImhvdXIiLCJkYXkiLCJpc1plcm8iLCJ2YWwiLCJ0aW1lckRheXMiLCJnZXRFbGVtZW50QnlJZCIsInRpbWVySG91cnMiLCJ0aW1lck1pbnV0ZXMiLCJ0aW1lclNlY29uZHMiLCJ4Iiwic2V0SW50ZXJ2YWwiLCJub3ciLCJkaXN0YW5jZSIsImlubmVyVGV4dCIsIk1hdGgiLCJmbG9vciIsImNsZWFySW50ZXJ2YWwiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQUFhQSxRQUFiO29CQUNjQyxPQUFaLEVBQXFCOzs7UUFDZixDQUFDQSxPQUFMLEVBQWM7U0FDVEMsR0FBTCxHQUFXRCxPQUFYO1NBQ0tFLEtBQUwsR0FBYSxLQUFLRCxHQUFMLENBQVNFLGFBQVQsQ0FBdUIsa0JBQXZCLENBQWI7U0FDS0MsS0FBTCxHQUFhLEtBQUtILEdBQUwsQ0FBU0UsYUFBVCxDQUF1QixrQkFBdkIsQ0FBYjtTQUNLRSxNQUFMLEdBQWMsS0FBS0osR0FBTCxDQUFTSyxnQkFBVCxDQUEwQixrQkFBMUIsQ0FBZDtTQUVLQyxPQUFMLEdBQWUsS0FBS04sR0FBTCxDQUFTTyxPQUFULENBQWlCLE1BQWpCLEtBQTRCLEtBQUtILE1BQUwsQ0FBWSxDQUFaLEVBQWVHLE9BQWYsQ0FBdUIsV0FBdkIsQ0FBNUIsSUFBbUUsQ0FBbEY7U0FDS0MsTUFBTCxHQUFjLEtBQWQ7U0FFS0MsS0FBTDs7Ozs7NEJBR007V0FDRFQsR0FBTCxDQUFTVSxnQkFBVCxDQUEwQixZQUExQixFQUF3QyxLQUFLQyxXQUFMLENBQWlCQyxJQUFqQixDQUFzQixJQUF0QixDQUF4QztXQUNLWixHQUFMLENBQVNVLGdCQUFULENBQTBCLFlBQTFCLEVBQXdDLEtBQUtDLFdBQUwsQ0FBaUJDLElBQWpCLENBQXNCLElBQXRCLENBQXhDOzs7O2lDQUdXO1VBQ1AsS0FBS0osTUFBVCxFQUFpQjthQUNWUixHQUFMLENBQVNhLFNBQVQsQ0FBbUJDLEdBQW5CLENBQXVCLFNBQXZCO09BREYsTUFFTzthQUNBZCxHQUFMLENBQVNhLFNBQVQsQ0FBbUJFLE1BQW5CLENBQTBCLFNBQTFCOzs7OztnQ0FJUUMsQ0EzQmQsRUEyQmlCO1dBQ1JSLE1BQUwsR0FBY1EsQ0FBQyxDQUFDQyxJQUFGLEtBQVcsWUFBWCxHQUEwQixJQUExQixHQUFpQyxLQUEvQztXQUNLQyxVQUFMOzs7Ozs7QUFJSixBQUFPLFNBQVNDLGNBQVQsR0FBMEI7RUFDL0JDLEtBQUssQ0FBQ0MsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQ0VDLFFBQVEsQ0FBQ25CLGdCQUFULENBQTBCLGFBQTFCLENBREYsRUFFRSxVQUFBb0IsSUFBSTtXQUFJLElBQUkzQixRQUFKLENBQWEyQixJQUFiLENBQUo7R0FGTjs7O0FDbENLLFNBQVNDLGVBQVQsR0FBMkI7RUFDaENDLE1BQU0sQ0FBQ0MsY0FBUCxHQUF3QixFQUF4QjtFQUNBUixLQUFLLENBQUNDLFNBQU4sQ0FBZ0JDLE9BQWhCLENBQXdCQyxJQUF4QixDQUNFQyxRQUFRLENBQUNuQixnQkFBVCxDQUEwQixvQkFBMUIsQ0FERixFQUVFLFVBQUFvQixJQUFJLEVBQUk7UUFDQUksRUFBRSxHQUFHLElBQUlDLE1BQUosQ0FBV0wsSUFBSSxDQUFDdkIsYUFBTCxDQUFtQixtQkFBbkIsQ0FBWCxFQUFvRDtNQUM3RDZCLElBQUksRUFBRSxLQUR1RDtNQUU3REMsWUFBWSxFQUFFLEVBRitDO01BRzdEQyxhQUFhLEVBQUUsQ0FIOEM7TUFJN0RDLFVBQVUsRUFBRTtRQUNWQyxNQUFNLEVBQUVWLElBQUksQ0FBQ3ZCLGFBQUwsQ0FBbUIsd0JBQW5CLENBREU7UUFFVmtDLE1BQU0sRUFBRVgsSUFBSSxDQUFDdkIsYUFBTCxDQUFtQix3QkFBbkI7O0tBTkQsQ0FBWDtJQVNBMEIsY0FBYyxDQUFDUyxJQUFmLENBQW9CUixFQUFwQjtHQVpKOztBQWlCRixBQUFPLFNBQVNTLHFCQUFULEdBQWlDO0VBQ3RDVixjQUFjLENBQUNOLE9BQWYsQ0FBdUIsVUFBQUcsSUFBSSxFQUFJO0lBQzdCQSxJQUFJLENBQUNjLE1BQUw7R0FERjs7O0lDcEJXQyxJQUFiO2dCQUNjekMsT0FBWixFQUFxQjs7O1FBQ2YsQ0FBQ0EsT0FBTCxFQUFjO1NBQ1RDLEdBQUwsR0FBV0QsT0FBWDtTQUNLRSxLQUFMLEdBQWEsS0FBS0QsR0FBTCxDQUFTRSxhQUFULENBQXVCLGtCQUF2QixDQUFiO1NBQ0tDLEtBQUwsR0FBYSxLQUFLSCxHQUFMLENBQVNFLGFBQVQsQ0FBdUIsa0JBQXZCLENBQWI7U0FDS3VDLEtBQUwsR0FBYSxLQUFLekMsR0FBTCxDQUFTSyxnQkFBVCxDQUEwQixpQkFBMUIsQ0FBYjtTQUNLcUMsT0FBTCxHQUFlLEtBQUsxQyxHQUFMLENBQVNLLGdCQUFULENBQTBCLG1CQUExQixDQUFmO1NBRUtDLE9BQUwsR0FBZSxLQUFLTixHQUFMLENBQVNPLE9BQVQsQ0FBaUIsTUFBakIsS0FBNEIsS0FBS2tDLEtBQUwsQ0FBVyxDQUFYLEVBQWNsQyxPQUFkLENBQXNCLEtBQXRCLENBQTVCLElBQTRELENBQTNFO1NBRUtFLEtBQUw7Ozs7OzRCQUdNOzs7V0FDRGlDLE9BQUwsR0FBZXRCLEtBQUssQ0FBQ3VCLElBQU4sQ0FBVyxLQUFLRCxPQUFoQixFQUF5QkUsTUFBekIsQ0FBZ0MsVUFBQ0MsR0FBRCxFQUFNaEIsRUFBTixFQUFhO1lBQ3BEaUIsRUFBRSxHQUFHakIsRUFBRSxDQUFDdEIsT0FBSCxDQUFXLFdBQVgsQ0FBWDtRQUNBc0MsR0FBRyxDQUFDQyxFQUFELENBQUgsR0FBVWpCLEVBQVY7ZUFDT2dCLEdBQVA7T0FIYSxFQUlaLEVBSlksQ0FBZjtXQUtLSixLQUFMLEdBQWFyQixLQUFLLENBQUN1QixJQUFOLENBQVcsS0FBS0YsS0FBaEIsRUFBdUJHLE1BQXZCLENBQThCLFVBQUNDLEdBQUQsRUFBTWhCLEVBQU4sRUFBYTtZQUNoRGlCLEVBQUUsR0FBR2pCLEVBQUUsQ0FBQ3RCLE9BQUgsQ0FBVyxTQUFYLENBQVg7UUFDQXNDLEdBQUcsQ0FBQ0MsRUFBRCxDQUFILEdBQVVqQixFQUFWO2VBQ09nQixHQUFQO09BSFcsRUFJVixFQUpVLENBQWI7TUFLQUUsTUFBTSxDQUFDQyxNQUFQLENBQWMsS0FBS1AsS0FBbkIsRUFBMEJuQixPQUExQixDQUFrQyxVQUFBTyxFQUFFLEVBQUk7UUFDdENBLEVBQUUsQ0FBQ25CLGdCQUFILENBQW9CLE9BQXBCLEVBQTZCLEtBQUksQ0FBQ0MsV0FBTCxDQUFpQkMsSUFBakIsQ0FBc0IsS0FBdEIsRUFBNEJpQixFQUE1QixDQUE3QjtPQURGO1dBR0tvQixhQUFMOzs7O29DQUdjO01BQ2RGLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEtBQUtQLEtBQW5CLEVBQTBCbkIsT0FBMUIsQ0FBa0MsVUFBQU8sRUFBRTtlQUFJQSxFQUFFLENBQUNoQixTQUFILENBQWFFLE1BQWIsQ0FBb0IsV0FBcEIsQ0FBSjtPQUFwQztNQUNBZ0MsTUFBTSxDQUFDQyxNQUFQLENBQWMsS0FBS04sT0FBbkIsRUFBNEJwQixPQUE1QixDQUFvQyxVQUFBTyxFQUFFO2VBQUlBLEVBQUUsQ0FBQ2hCLFNBQUgsQ0FBYUUsTUFBYixDQUFvQixXQUFwQixDQUFKO09BQXRDO1VBQ0ksS0FBSzBCLEtBQUwsQ0FBVyxLQUFLbkMsT0FBaEIsQ0FBSixFQUE4QixLQUFLbUMsS0FBTCxDQUFXLEtBQUtuQyxPQUFoQixFQUF5Qk8sU0FBekIsQ0FBbUNDLEdBQW5DLENBQXVDLFdBQXZDO1VBQzFCLEtBQUs0QixPQUFMLENBQWEsS0FBS3BDLE9BQWxCLENBQUosRUFBZ0MsS0FBS29DLE9BQUwsQ0FBYSxLQUFLcEMsT0FBbEIsRUFBMkJPLFNBQTNCLENBQXFDQyxHQUFyQyxDQUF5QyxXQUF6Qzs7OztnQ0FHdEJlLEVBdENkLEVBc0NrQmIsQ0F0Q2xCLEVBc0NxQjtVQUNYOEIsRUFBRSxHQUFHakIsRUFBRSxDQUFDdEIsT0FBSCxDQUFXLFNBQVgsQ0FBWDtVQUNJLENBQUN1QyxFQUFMLEVBQVM7V0FDSnhDLE9BQUwsR0FBZXdDLEVBQWY7V0FDS0csYUFBTDs7Ozs7O0FBSUosQUFBTyxTQUFTQyxjQUFULEdBQTBCO0VBQy9COUIsS0FBSyxDQUFDQyxTQUFOLENBQWdCQyxPQUFoQixDQUF3QkMsSUFBeEIsQ0FDRUMsUUFBUSxDQUFDbkIsZ0JBQVQsQ0FBMEIsYUFBMUIsQ0FERixFQUVFLFVBQUFvQixJQUFJO1dBQUksSUFBSWUsSUFBSixDQUFTZixJQUFULENBQUo7R0FGTjs7O0FDL0NLLFNBQVMwQixjQUFULEdBQTBCO0VBQy9CL0IsS0FBSyxDQUFDQyxTQUFOLENBQWdCQyxPQUFoQixDQUF3QkMsSUFBeEIsQ0FDRUMsUUFBUSxDQUFDbkIsZ0JBQVQsQ0FBMEIsa0JBQTFCLENBREYsRUFFRSxVQUFBb0IsSUFBSSxFQUFJO1FBQ0ZLLE1BQUosQ0FBV0wsSUFBSSxDQUFDdkIsYUFBTCxDQUFtQixtQkFBbkIsQ0FBWCxFQUFvRDtNQUNsRDZCLElBQUksRUFBRSxLQUQ0QztNQUVsREMsWUFBWSxFQUFFLENBRm9DO01BR2xEQyxhQUFhLEVBQUUsQ0FIbUM7TUFJbERtQixNQUFNLEVBQUUsTUFKMEM7TUFLbERsQixVQUFVLEVBQUU7UUFDVkMsTUFBTSxFQUFFVixJQUFJLENBQUN2QixhQUFMLENBQW1CLHdCQUFuQixDQURFO1FBRVZrQyxNQUFNLEVBQUVYLElBQUksQ0FBQ3ZCLGFBQUwsQ0FBbUIsd0JBQW5COztLQVBaO0dBSEo7OztJQ0RXbUQsR0FBYjtlQUNjdEQsT0FBWixFQUFxQjs7O1FBQ2YsQ0FBQ0EsT0FBTCxFQUFjO1NBQ1RDLEdBQUwsR0FBV0QsT0FBWDtTQUNLRSxLQUFMLEdBQWEsS0FBS0QsR0FBTCxDQUFTRSxhQUFULENBQXVCLGlCQUF2QixDQUFiO1NBQ0tDLEtBQUwsR0FBYSxLQUFLSCxHQUFMLENBQVNFLGFBQVQsQ0FBdUIsaUJBQXZCLENBQWI7U0FFS00sTUFBTCxHQUFjLEtBQUtSLEdBQUwsQ0FBU08sT0FBVCxDQUFpQixLQUFqQixLQUEyQixNQUEzQixHQUFvQyxJQUFwQyxHQUEyQyxLQUF6RDtTQUVLRSxLQUFMOzs7Ozs0QkFHTTtXQUNEVCxHQUFMLENBQVNVLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLEtBQUs0QyxXQUFMLENBQWlCMUMsSUFBakIsQ0FBc0IsSUFBdEIsQ0FBbkM7V0FDS00sVUFBTDs7OztpQ0FHVztVQUNQLEtBQUtWLE1BQVQsRUFBaUI7YUFDVlIsR0FBTCxDQUFTYSxTQUFULENBQW1CQyxHQUFuQixDQUF1QixXQUF2QjtPQURGLE1BRU87YUFDQWQsR0FBTCxDQUFTYSxTQUFULENBQW1CRSxNQUFuQixDQUEwQixXQUExQjs7Ozs7Z0NBSVFDLENBekJkLEVBeUJpQjtXQUNSUixNQUFMLEdBQWMsS0FBS0EsTUFBTCxHQUFjLEtBQWQsR0FBc0IsSUFBcEM7V0FDS1UsVUFBTDs7Ozs7O0FBSUosQUFBTyxTQUFTcUMsYUFBVCxHQUF5QjtFQUM5Qm5DLEtBQUssQ0FBQ0MsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQ0VDLFFBQVEsQ0FBQ25CLGdCQUFULENBQTBCLFlBQTFCLENBREYsRUFFRSxVQUFBb0IsSUFBSTtXQUFJLElBQUk0QixHQUFKLENBQVE1QixJQUFSLENBQUo7R0FGTjs7O0FDaENLLFNBQVMrQixnQkFBVCxHQUE0QjtFQUNqQ3BDLEtBQUssQ0FBQ0MsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQ0VDLFFBQVEsQ0FBQ25CLGdCQUFULENBQTBCLG9CQUExQixDQURGLEVBRUUsVUFBQW9CLElBQUksRUFBSTtRQUNGSyxNQUFKLENBQVdMLElBQVgsRUFBaUI7TUFDZk0sSUFBSSxFQUFFLEtBRFM7TUFFZkMsWUFBWSxFQUFFLENBRkM7TUFHZkMsYUFBYSxFQUFFLENBSEE7TUFJZm1CLE1BQU0sRUFBRSxNQUpPO01BS2ZsQixVQUFVLEVBQUU7UUFDVkMsTUFBTSxFQUFFVixJQUFJLENBQUN2QixhQUFMLENBQW1CLHdCQUFuQixDQURFO1FBRVZrQyxNQUFNLEVBQUVYLElBQUksQ0FBQ3ZCLGFBQUwsQ0FBbUIsd0JBQW5COztLQVBaO0dBSEo7OztBQ0RLLFNBQVN1RCxpQkFBVCxHQUE2QjtFQUNsQ3JDLEtBQUssQ0FBQ0MsU0FBTixDQUFnQkMsT0FBaEIsQ0FBd0JDLElBQXhCLENBQ0VDLFFBQVEsQ0FBQ25CLGdCQUFULENBQTBCLGtCQUExQixDQURGLEVBRUUsVUFBQW9CLElBQUksRUFBSTtJQUNOaUMsWUFBWSxDQUFDakMsSUFBRCxDQUFaO0dBSEo7OztJQ0RXa0MsUUFBYjtvQkFDY0MsV0FBWixFQUF5Qjs7O1NBQ2xCQyxJQUFMLEdBQVk7TUFDVkMsV0FBVyxFQUFFLE1BREg7TUFFVkMsY0FBYyxFQUFFLE1BRk47TUFHVkMsWUFBWSxFQUFFLEdBSEo7TUFJVkMsY0FBYyxFQUFFO0tBSmxCO1NBTUtDLElBQUwsR0FBWTFDLFFBQVEsQ0FBQ25CLGdCQUFULENBQTBCdUQsV0FBMUIsQ0FBWjtTQUNLTyxNQUFMLEdBQWMzQyxRQUFRLENBQUNuQixnQkFBVCxDQUEwQixjQUExQixDQUFkO1NBQ0srRCxTQUFMLEdBQWlCNUMsUUFBUSxDQUFDbkIsZ0JBQVQsQ0FBMEIsb0JBQTFCLENBQWpCO1NBQ0tnRSxzQkFBTCxHQUE4QixJQUE5QjtTQUVLQyxJQUFMOzs7OzsyQkFHSzs7V0FFQUMsTUFBTDtXQUNLVixJQUFMLENBQVVJLGNBQVYsR0FBMkIsS0FBS0EsY0FBTCxFQUEzQjs7Ozs2QkFHTzs7O1NBQ0ozQyxPQUFILENBQVdDLElBQVgsQ0FBZ0IsS0FBSzJDLElBQXJCLEVBQTJCLFVBQUNNLEdBQUQsRUFBUztRQUNsQ0EsR0FBRyxDQUFDOUQsZ0JBQUosQ0FBcUIsT0FBckIsRUFBOEIsVUFBQ00sQ0FBRDtpQkFDNUIsS0FBSSxDQUFDeUQsU0FBTCxDQUFlRCxHQUFHLENBQUNqRSxPQUFKLENBQVltRSxXQUEzQixFQUF3QzFELENBQXhDLENBRDRCO1NBQTlCO09BREY7U0FNR00sT0FBSCxDQUFXQyxJQUFYLENBQWdCLEtBQUs2QyxTQUFyQixFQUFnQyxVQUFDSSxHQUFELEVBQVM7UUFDdkNBLEdBQUcsQ0FBQzlELGdCQUFKLENBQXFCLE9BQXJCLEVBQThCLFVBQUNNLENBQUQ7aUJBQzVCLEtBQUksQ0FBQzJELFVBQUwsQ0FBZ0JILEdBQUcsQ0FBQ0ksT0FBSixDQUFZLGNBQVosQ0FBaEIsRUFBNkM1RCxDQUE3QyxDQUQ0QjtTQUE5QjtPQURGO1NBTUdNLE9BQUgsQ0FBV0MsSUFBWCxDQUFnQixLQUFLNEMsTUFBckIsRUFBNkIsVUFBQ1UsS0FBRCxFQUFXO1FBQ3RDQSxLQUFLLENBQUNuRSxnQkFBTixDQUF1QixPQUF2QixFQUFnQyxVQUFDTSxDQUFEO2lCQUFPLEtBQUksQ0FBQzhELFFBQUwsQ0FBY0QsS0FBZCxFQUFxQjdELENBQXJCLENBQVA7U0FBaEM7T0FERjtNQUdBUSxRQUFRLENBQUNkLGdCQUFULENBQTBCLFNBQTFCLEVBQXFDLEtBQUtxRSxtQkFBMUM7Ozs7OEJBR1FMLFdBekNaLEVBeUN5Qk0sS0F6Q3pCLEVBeUNnQztVQUN0QkgsS0FBSyxHQUFHckQsUUFBUSxDQUFDdEIsYUFBVCx3QkFBdUN3RSxXQUF2QyxRQUFkO1VBQ0ksQ0FBQ0csS0FBTCxFQUFZO1dBQ1BSLHNCQUFMLEdBQThCN0MsUUFBUSxDQUFDeUQsYUFBdkM7TUFDQUosS0FBSyxDQUFDaEUsU0FBTixDQUFnQkMsR0FBaEIsQ0FBb0IsS0FBSytDLElBQUwsQ0FBVUMsV0FBOUI7TUFDQWUsS0FBSyxDQUFDSyxZQUFOLENBQW1CLFVBQW5CLEVBQStCLENBQS9CO01BQ0FMLEtBQUssQ0FBQ00sS0FBTjtNQUNBM0QsUUFBUSxDQUFDNEQsSUFBVCxDQUFjQyxLQUFkLENBQW9CQyxRQUFwQixHQUErQixRQUEvQjtNQUNBOUQsUUFBUSxDQUFDNEQsSUFBVCxDQUFjQyxLQUFkLENBQW9CRSxXQUFwQixhQUFxQyxLQUFLMUIsSUFBTCxDQUFVSSxjQUEvQzs7OzsrQkFHU1ksS0FwRGIsRUFvRG9CRyxLQXBEcEIsRUFvRDJCOzs7TUFDdkJILEtBQUssQ0FBQ2hFLFNBQU4sQ0FBZ0JDLEdBQWhCLENBQW9CLEtBQUsrQyxJQUFMLENBQVVFLGNBQTlCO1dBQ0tNLHNCQUFMLENBQTRCYyxLQUE1QjtNQUNBSyxVQUFVLENBQUMsWUFBTTtRQUNmWCxLQUFLLENBQUNoRSxTQUFOLENBQWdCRSxNQUFoQixDQUF1QixNQUFJLENBQUM4QyxJQUFMLENBQVVDLFdBQWpDLEVBQThDLE1BQUksQ0FBQ0QsSUFBTCxDQUFVRSxjQUF4RDtRQUNBdkMsUUFBUSxDQUFDNEQsSUFBVCxDQUFjQyxLQUFkLENBQW9CQyxRQUFwQixHQUErQixTQUEvQjtRQUNBOUQsUUFBUSxDQUFDNEQsSUFBVCxDQUFjQyxLQUFkLENBQW9CRSxXQUFwQixHQUFrQyxHQUFsQztRQUNBVixLQUFLLENBQUNZLGVBQU4sQ0FBc0IsVUFBdEI7T0FKUSxFQUtQLEtBQUs1QixJQUFMLENBQVVHLFlBTEgsQ0FBVjs7OztvQ0FRYzs7O1NBQ1gxQyxPQUFILENBQVdDLElBQVgsQ0FBZ0IsS0FBSzRDLE1BQXJCLEVBQTZCLFVBQUNVLEtBQUQsRUFBVztRQUN0QyxNQUFJLENBQUNGLFVBQUwsQ0FBZ0JFLEtBQWhCO09BREY7Ozs7NkJBS09BLEtBckVYLEVBcUVrQkcsS0FyRWxCLEVBcUV5QjtVQUNqQkEsS0FBSyxDQUFDVSxNQUFOLEtBQWlCVixLQUFLLENBQUNXLGFBQTNCLEVBQTBDO2FBQ25DaEIsVUFBTCxDQUFnQkUsS0FBaEI7Ozs7O3dDQUlnQkcsS0EzRXRCLEVBMkU2QjtVQUNyQkEsS0FBSyxDQUFDWSxPQUFOLEtBQWtCLEVBQXRCLEVBQTBCO2FBQ25CQyxhQUFMOzs7OztxQ0FJYTtVQUNYQyxHQUFHLEdBQUd0RSxRQUFRLENBQUN1RSxhQUFULENBQXVCLEtBQXZCLENBQVY7TUFFQUQsR0FBRyxDQUFDVCxLQUFKLENBQVVXLFNBQVYsR0FBc0IsUUFBdEI7TUFDQUYsR0FBRyxDQUFDVCxLQUFKLENBQVVZLEtBQVYsR0FBa0IsTUFBbEI7TUFDQUgsR0FBRyxDQUFDVCxLQUFKLENBQVVhLE1BQVYsR0FBbUIsTUFBbkIsQ0FMZTs7TUFRZjFFLFFBQVEsQ0FBQzRELElBQVQsQ0FBY2UsTUFBZCxDQUFxQkwsR0FBckI7VUFDSU0sV0FBVyxHQUFHTixHQUFHLENBQUNPLFdBQUosR0FBa0JQLEdBQUcsQ0FBQ1EsV0FBeEM7TUFFQVIsR0FBRyxDQUFDL0UsTUFBSjthQUVPcUYsV0FBUDs7Ozs7O0FBS0osQUFBTyxTQUFTRyxZQUFULEdBQXdCO0VBQzdCNUUsTUFBTSxDQUFDNkUsUUFBUCxHQUFrQixJQUFJN0MsUUFBSixDQUFhLHFCQUFiLENBQWxCOzs7QUNwR0ssU0FBUzhDLGVBQVQsR0FBMkI7TUFDMUJDLFVBQVUsR0FBR2xGLFFBQVEsQ0FBQ3RCLGFBQVQsQ0FBdUIsZUFBdkIsQ0FBbkI7TUFDTXVHLGVBQWUsR0FBR2pGLFFBQVEsQ0FBQ3RCLGFBQVQsQ0FBdUIsb0JBQXZCLENBQXhCO0VBRUF1RyxlQUFlLENBQUMvRixnQkFBaEIsQ0FBaUMsT0FBakMsRUFBMEMsWUFBTTtRQUMxQ2dHLFVBQVUsQ0FBQzdGLFNBQVgsQ0FBcUI4RixRQUFyQixDQUE4QixXQUE5QixDQUFKLEVBQWdEO01BQzlDbkYsUUFBUSxDQUFDNEQsSUFBVCxDQUFjQyxLQUFkLENBQW9CQyxRQUFwQixHQUErQixFQUEvQjtLQURGLE1BRU87TUFDTDlELFFBQVEsQ0FBQzRELElBQVQsQ0FBY0MsS0FBZCxDQUFvQkMsUUFBcEIsR0FBK0IsUUFBL0I7OztJQUVGbUIsZUFBZSxDQUFDNUYsU0FBaEIsQ0FBMEIrRixNQUExQixDQUFpQyxXQUFqQztJQUNBRixVQUFVLENBQUM3RixTQUFYLENBQXFCK0YsTUFBckIsQ0FBNEIsV0FBNUI7R0FQRjs7O0FDSkssU0FBU0MsY0FBVCxHQUEwQjtNQUN6QkMsU0FBUyxHQUFHLElBQUlDLElBQUosQ0FBUyxJQUFULEVBQWUsQ0FBZixFQUFrQixFQUFsQixFQUFzQixFQUF0QixFQUEwQixDQUExQixFQUE2QixDQUE3QixFQUFnQ0MsT0FBaEMsRUFBbEI7TUFDTUMsTUFBTSxHQUFHLElBQWY7TUFDTUMsTUFBTSxHQUFHRCxNQUFNLEdBQUcsRUFEeEI7TUFFTUUsSUFBSSxHQUFHRCxNQUFNLEdBQUcsRUFGdEI7TUFHTUUsR0FBRyxHQUFHRCxJQUFJLEdBQUcsRUFIbkI7O01BS01FLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUNDLEdBQUQ7V0FBU0EsR0FBRyxHQUFHLEVBQU4sR0FBVyxNQUFNQSxHQUFqQixHQUF1QkEsR0FBaEM7R0FBZjs7TUFFTUMsU0FBUyxHQUFHL0YsUUFBUSxDQUFDZ0csY0FBVCxDQUF3QixXQUF4QixDQUFsQjtNQUNNQyxVQUFVLEdBQUdqRyxRQUFRLENBQUNnRyxjQUFULENBQXdCLFlBQXhCLENBQW5CO01BQ01FLFlBQVksR0FBR2xHLFFBQVEsQ0FBQ2dHLGNBQVQsQ0FBd0IsY0FBeEIsQ0FBckI7TUFDTUcsWUFBWSxHQUFHbkcsUUFBUSxDQUFDZ0csY0FBVCxDQUF3QixjQUF4QixDQUFyQjtNQUVJLENBQUNELFNBQUQsSUFBYyxDQUFDRSxVQUFmLElBQTZCLENBQUNDLFlBQTlCLElBQThDLENBQUNDLFlBQW5ELEVBQWlFO01BRTdEQyxDQUFDLEdBQUdDLFdBQVcsQ0FBQyxZQUFXO1FBRTNCQyxHQUFHLEdBQUcsSUFBSWYsSUFBSixHQUFXQyxPQUFYLEVBQVY7UUFDSWUsUUFBUSxHQUFHakIsU0FBUyxHQUFHZ0IsR0FBM0I7SUFFQVAsU0FBUyxDQUFDUyxTQUFWLEdBQXNCWCxNQUFNLENBQUNZLElBQUksQ0FBQ0MsS0FBTCxDQUFXSCxRQUFRLEdBQUlYLEdBQXZCLENBQUQsQ0FBNUI7SUFDQUssVUFBVSxDQUFDTyxTQUFYLEdBQXVCWCxNQUFNLENBQUNZLElBQUksQ0FBQ0MsS0FBTCxDQUFZSCxRQUFRLEdBQUlYLEdBQWIsR0FBc0JELElBQWpDLENBQUQsQ0FBN0I7SUFDQU8sWUFBWSxDQUFDTSxTQUFiLEdBQXlCWCxNQUFNLENBQUNZLElBQUksQ0FBQ0MsS0FBTCxDQUFZSCxRQUFRLEdBQUlaLElBQWIsR0FBdUJELE1BQWxDLENBQUQsQ0FBL0I7SUFDQVMsWUFBWSxDQUFDSyxTQUFiLEdBQXlCWCxNQUFNLENBQUNZLElBQUksQ0FBQ0MsS0FBTCxDQUFZSCxRQUFRLEdBQUliLE1BQWIsR0FBd0JELE1BQW5DLENBQUQsQ0FBL0I7O1FBRUljLFFBQVEsR0FBRyxDQUFmLEVBQWtCO01BQ2hCSSxhQUFhLENBQUNQLENBQUQsQ0FBYjs7R0FYaUIsRUFjaEJYLE1BZGdCLENBQW5COzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzsifQ==
