import gulp from 'gulp'
import bSync from 'browser-sync'
import del from 'del'
import rename from 'gulp-rename'
import concat from 'gulp-concat'
import rollup from 'gulp-rollup'
import babel from 'rollup-plugin-babel'
import pug from 'gulp-pug'
import sass from 'gulp-sass'
import autoprefixer from 'gulp-autoprefixer'
import sourcemaps from 'gulp-sourcemaps'
import csso from 'gulp-csso'

const browserSync = bSync.create()
const paths = {
  dist: "./docs",
  assets: "./docs/assets",
  src: "./src"
}


// CLEAN
export const cleanTask = () => {
  return del(paths.dist + '/')
}

// SERVER
export const serverTask = () => {
  return browserSync.init({
    server: paths.dist,
    https: false,
    open: false
  });
}

// PUBLIC
export const publicTask = () => {
  return gulp.src(paths.src + "/public/**/*")
    .pipe(gulp.dest(paths.dist));
}

// PUG
export const pugTask = () => {
  return gulp.src(paths.src + '/views/**/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(paths.dist))
    .on('end', browserSync.reload)
}

// STYLE
export const styleTask_Lib = () => {
  return gulp.src(paths.src + '/libs/**/*.css')
    .pipe(concat('libs.min.css'))
    .pipe(csso())
    .pipe(gulp.dest(paths.assets + '/css/'))
    .pipe(browserSync.reload({
      stream: true
    }));
}
export const styleTask_Dev = () => {
  return gulp.src(paths.src + '/sass/index.sass')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
      overrideBrowserslist:  ['last 10 versions']
    }))
    .pipe(sourcemaps.write())
    .pipe(rename('main.css'))
    .pipe(gulp.dest(paths.assets + '/css/'))
    .pipe(browserSync.reload({
      stream: true
    }));
}

// SCRIPT
export const scriptTask_Lib = () => {
  return gulp.src(paths.src + '/libs/**/*.js')
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest(paths.assets + '/js/'))
    .pipe(browserSync.reload({
      stream: true
    }));
}
export const scriptTask_Dev = () => {
  return gulp.src(paths.src + '/js/index.js')
    .pipe(sourcemaps.init())
    .pipe(rollup({
      allowRealFiles: true,
      input: './src/js/index.js',
      output: {
        file: 'main.js',
        format: 'cjs',
        sourcemap: 'inline'
      },
      plugins: [
        babel({
          exclude: 'node_modules/**'
        })
      ],
    }))
    .pipe(rename('main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.assets + '/js/'))
    .pipe(browserSync.reload({
      stream: true
    }));
}

// WATCH
export const watchTask = () => {
  gulp.watch(paths.src + "/public/**/*", publicTask);
  // gulp.watch(paths.src + "/public/fonts/**/*.*", publicTask);
  // gulp.watch(paths.src + "/public/video/*.{mp4,webm}", publicTask);
  // gulp.watch(paths.src + "/public/images/**/*.{png,svg,jpg,jpeg,gif}", publicTask);
  gulp.watch(
    [
      paths.src + "/views/**/*.pug",
      paths.src + "/layouts/**/*.pug",
      paths.src + "/components/**/*.pug",
    ],
    pugTask
  );
  gulp.watch(paths.src + "/libs/**/*.css", styleTask_Lib);
  gulp.watch(
    [
      paths.src + "/sass/**/*.sass",
      paths.src + "/components/**/*.sass"
    ], styleTask_Dev
  );
  gulp.watch(paths.src + "/libs/**/*.js", scriptTask_Lib);
  // gulp.watch(paths.src + "/public/json/*.json", publicTask);
  gulp.watch(paths.src + "/components/**/*.js", scriptTask_Dev);
}

//DEV
export const devTask = gulp.series(
  cleanTask,
  gulp.parallel(
    publicTask,
    pugTask,
    styleTask_Lib,
    styleTask_Dev,
    scriptTask_Lib,
    scriptTask_Dev
  )
)

export default gulp.series(
  devTask,
  gulp.parallel(
    watchTask,
    serverTask
  )
)
