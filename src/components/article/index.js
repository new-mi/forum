export function articleSliderAll() {
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-slider-article'),
    item => {
      new Swiper(item, {
        loop: false,
        spaceBetween: 0,
        slidesPerView: 1,
        effect: 'fade',
        navigation: {
          nextEl: item.querySelector('.slider-nav__item_next'),
          prevEl: item.querySelector('.slider-nav__item_prev'),
        },
      })
    }
  )
}
