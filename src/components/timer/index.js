export function countDownTimer() {
  const countDown = new Date(2020, 8, 20, 12, 0, 0).getTime()
  const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

  const isZero = (val) => val < 10 ? '0' + val : val

  const timerDays = document.getElementById('timerDays')
  const timerHours = document.getElementById('timerHours')
  const timerMinutes = document.getElementById('timerMinutes')
  const timerSeconds = document.getElementById('timerSeconds')

  if (!timerDays && !timerHours && !timerMinutes && !timerSeconds) return;

  let x = setInterval(function() {

  let now = new Date().getTime()
  let distance = countDown - now;

  timerDays.innerText = isZero(Math.floor(distance / (day)))
  timerHours.innerText = isZero(Math.floor((distance % (day)) / (hour)))
  timerMinutes.innerText = isZero(Math.floor((distance % (hour)) / (minute)))
  timerSeconds.innerText = isZero(Math.floor((distance % (minute)) / second))

  if (distance < 0) {
    clearInterval(x);
  }

  }, second)
}
