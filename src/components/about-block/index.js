export function aboutSliderAll() {
  Array.prototype.forEach.call(
    document.querySelectorAll('.js-slider-about'),
    item => {
      new Swiper(item.querySelector('.swiper-container'), {
        loop: false,
        spaceBetween: 0,
        slidesPerView: 1,
        effect: 'fade',
        navigation: {
          nextEl: item.querySelector('.slider-nav__item_next'),
          prevEl: item.querySelector('.slider-nav__item_prev'),
        },
      })
    }
  )
}
