export class Faq {
  constructor(element) {
    if (!element) return
    this.$el = element
    this.$head = this.$el.querySelector('[data-faq-head]')
    this.$body = this.$el.querySelector('[data-faq-body]')

    this.isShow = this.$el.dataset['faq'] == 'true' ? true : false

    this.setup()
  }

  setup() {
    this.$el.addEventListener('click', this.handleClick.bind(this))
    this.updateShow()
  }

  updateShow() {
    if (this.isShow) {
      this.$el.classList.add('is-active')
    } else {
      this.$el.classList.remove('is-active')
    }
  }

  handleClick(e) {
    this.isShow = this.isShow ? false : true
    this.updateShow()
  }
}

export function installAllFaq() {
  Array.prototype.forEach.call(
    document.querySelectorAll('[data-faq]'),
    item => new Faq(item)
  )
}
