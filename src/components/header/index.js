export function headerHumburger() {
  const headerMenu = document.querySelector('.header__menu');
  const headerHumburger = document.querySelector('.header__humburger');

  headerHumburger.addEventListener('click', () => {
    if (headerMenu.classList.contains('is-active')) {
      document.body.style.overflow = ''
    } else {
      document.body.style.overflow = 'hidden'
    }
    headerHumburger.classList.toggle('is-active')
    headerMenu.classList.toggle('is-active')
  })
}
