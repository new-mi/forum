import { installAllDdwn } from '../components/dropdown/index'
import { expertSliderAll, updateExpertSliderAll } from '../components/experts-slider/index'
import { installAllTabs } from '../components/tabs/index'
import { aboutSliderAll } from '../components/about-block/index'
import { installAllFaq } from '../components/faq/index'
import { articleSliderAll } from '../components/article/index'
import { installAllGallery } from '../components/gallery/index'
import { installModal } from '../components/modal/index'
import { headerHumburger } from '../components/header/index'
import { countDownTimer } from '../components/timer/index'

window.addEventListener('DOMContentLoaded', (event) => {
  installAllDdwn()
  installAllTabs()
  aboutSliderAll()
  installAllFaq()
  articleSliderAll()
  installAllGallery()
  installModal()
  headerHumburger()
  countDownTimer()

  setTimeout(() => {
    expertSliderAll()
  }, 0)

  Array.prototype.forEach.call(
    document.querySelectorAll('[data-modal-target="live"]'),
    item => {
      item.addEventListener('click', () => {
        updateExpertSliderAll()
      })
  })
  Array.prototype.forEach.call(
    document.querySelectorAll('[data-tabs-tab]'),
    item => {
      item.addEventListener('click', () => {
        updateExpertSliderAll()
      })
  })
});
